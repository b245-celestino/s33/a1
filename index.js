fetch("https://jsonplaceholder.typicode.com/todos").then((Response) =>
  Response.json().then((data) => {
    let title = data.map((element) => element.title);
    console.log(title);
  })
);

fetch("https://jsonplaceholder.typicode.com/todos/1", { method: "GET" })
  .then((response) => response.json())
  .then((result) => console.log(result));

fetch("https://jsonplaceholder.typicode.com/todos/1", { method: "GET" })
  .then((response) => response.json())
  .then((result) =>
    console.log(
      `The item ${result.title} on the list has a status of ${result.completed}`
    )
  );

fetch("https://jsonplaceholder.typicode.com/todos", {
  method: "POST",
  headers: {
    "content-type": "application/json",
  },
  body: JSON.stringify({
    title: "Created to  do list",
    completed: false,
    usedId: 1,
  }),
})
  .then((response) => response.json())
  .then((result) => console.log(result));

fetch("https://jsonplaceholder.typicode.com/todos/5", {
  method: "PUT",
  headers: {
    "content-type": "application/json",
  },
  body: JSON.stringify({
    dateCompleted: "Pending",
    description: "To update the my to do list with a different data structure",
    status: "Pending",
    title: "Updated to Do List item",
    userId: 1,
  }),
})
  .then((response) => response.json())
  .then((result) => console.log(result));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PATCH",
  headers: {
    "content-type": "application/json",
  },
  body: JSON.stringify({
    dateCompleted: "07/09/21",
    status: "Complete",

    userId: 1,
  }),
})
  .then((response) => response.json())
  .then((result) => console.log(result));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "DELETE",
})
  .then((response) => response.json())
  .then((result) => console.log(result));
